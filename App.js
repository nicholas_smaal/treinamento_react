import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import { CONDOR_BG_LIGHT_1, CONDOR_BLUE } from './components/ui/condorColors';

import DataGenerator from './components/logic/DataGenerator';
import eventsModule from './components/logic/events';

import { Chart, Line, HorizontalAxis, VerticalAxis } from 'react-native-responsive-linechart';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    console.log('Aplicativo inicializando!');

    this.state = {
      count: 0,
      data: [
        { x: 0, y: 10 },
        { x: 1, y: 20 },
      ],
    };

    this.maxX = 50;

    this.DataGenerator = new DataGenerator();

    this.updateCount = this.updateCount.bind(this);

    this._isMounted = false;

  }
  componentDidMount() {
    console.log('Aplicativo montado!');
    this.DataGenerator.initDataGeneration();
    eventsModule.subscribe('newData', (newData) => {
      if (this._isMounted) {
        this.setState({ ...this.state, data: [...newData] });
      }
    });

    this._isMounted = true;
  }
  componentDidUpdate(prevProps, prevState, snapshot) {

    if (this.state.count !== prevState.count) {
      console.log(`New count : ${this.state.count}`);
    }
    if (this.state.data !== prevState.data) {
      console.log('New data : ' + JSON.stringify(this.state.data[this.state.data.length - 1]));
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    this.DataGenerator.stopDataGerenation();
    console.log('Aplicativo fechando!');
  }
  updateCount() {
    if (this._isMounted) {
      this.setState({ ...this.state, count: this.state.count + 1 });
    }
  }

  render() {
    if (this.state.data[this.state.data.length - 1].x > this.maxX) {
      this.maxX = this.state.data[this.state.data.length - 1].x + 25;
    }

    return (
      <ScrollView style={styles.mainContainer}>

        <View style={styles.imgLogoCont}>
          <Text style={styles.title}>
            {'\n'}Treinamento React
          </Text>
        </View>

        <View style={styles.mainContent}>

          <Text style={styles.logs}>
            Número de Cliques: {this.state.count}
          </Text>

          <TouchableOpacity
            onPress={() => {
              this.setState({ ...this.state, count: this.state.count + 1 });
            }}>
            <Text style={styles.dataBtn}>Contar</Text>
          </TouchableOpacity>

        </View>

        { this._isMounted ? (
          <View>
            <Chart
              style={styles.chart}
              data={this.state.data}
              padding={{ left: 40, bottom: 20, right: 20, top: 20 }}
              xDomain={{ min: 0, max: this.maxX }}
              yDomain={{ min: -60, max: 60 }}
            >
              <VerticalAxis tickCount={11} theme={{ labels: { formatter: (v) => v.toFixed(2) } }} />
              <HorizontalAxis tickCount={5} />
              <Line theme={{ stroke: { color: '#ffa502', width: 5 }, scatter: { default: { width: 4, height: 4, rx: 2 } } }} />
            </Chart>

            <TouchableOpacity
              onPress={() => {
                this.DataGenerator.clearData();
              }}>
              <Text style={styles.dataBtn}>Limpar Dados</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.DataGenerator.freqDouble();
              }}>
              <Text style={styles.dataBtn}>Aumentar Frequência</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.DataGenerator.freqHalf();
              }}>
              <Text style={styles.dataBtn}>Reduzir Frequência</Text>
            </TouchableOpacity>

          </View>

        ) :
          (
            <View />
          )}

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  mainContent: {
    flex: 1,
    paddingTop: 20,
  },
  imgLogoCont: {
    alignItems: 'center',
    marginTop: 100,
  },
  imgLogo: {
    resizeMode: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 25,
    fontWeight: 'bold',
    color: CONDOR_BLUE,
    marginBottom: 10,
    marginTop: -75,
  },
  idArea: {
    flexDirection: 'row',
    backgroundColor: CONDOR_BG_LIGHT_1,
    justifyContent: 'flex-start',
  },
  logContainer: {
    flex: 1,
    backgroundColor: CONDOR_BG_LIGHT_1,
    marginBottom: 10,
    marginTop: 10,
  },
  logs: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 10,
    marginTop: 10,
  },
  dataBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: CONDOR_BLUE,
    color: 'white',
    margin: 13,
    padding: 10,
    borderRadius: 10,
  },
  chart: {
    height: 200,
    width: 400,
    backgroundColor: '#eee',
  },
});
