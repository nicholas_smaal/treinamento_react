class EventsModule {
    constructor() {
        this.events = {};
    }

    reset() {
        this.events = {};
    }

    dispatch (event, data) {
        if (!this.events[event]) {return;}
        this.events[event].forEach((callback,i) => {
            callback(data);
        });
    }

    subscribe (event, callback) {
        if (!this.events[event]) {this.events[event] = [];}
        this.events[event].push(callback);
    }
}

const eventsModule = new EventsModule();

export default eventsModule;
