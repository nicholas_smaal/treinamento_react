// File: DataGenerator.js
import eventsModule from './events';

export default class DataGenerator {
    constructor() {
        this.data = [{ x: 0, y: 0 }];
        this.mult = 25;
        this.freq = 1;
        this.lastTime = 1;

        // simulador e geração de dados + plot deles separado

        this.dataGenerationInterval = 500;
        this.intervalId = null;

        this.initDataGeneration = this.initDataGeneration.bind(this);
        this.stopDataGerenation = this.stopDataGerenation.bind(this);
        this.updateData = this.updateData.bind(this);
        this.getData = this.getData.bind(this);
        this.clearData = this.clearData.bind(this);
        this.freqDouble = this.freqDouble.bind(this);
        this.freqHalf = this.freqHalf.bind(this);
    }

    initDataGeneration() {
        this.intervalId = setInterval(this.updateData, this.dataGenerationInterval);
    }

    stopDataGerenation() {
        clearInterval(this.intervalId);
    }

    // Data Operation
    updateData() {
        let d = { x: this.lastTime, y: Math.round(this.mult * Math.sin(this.lastTime * this.freq)) };
        this.data.push(d);
        this.lastTime += 1;
        eventsModule.dispatch('newData', this.getData());
    }

    getData() {
        return this.data;
    }

    clearData() {
        this.data = [{ x: 0, y: 0 }];
        this.lastTime = 1;
        console.log('Dados apagados!');
    }

    freqDouble() {
        if ((1000 * this.freq) < (3 * this.dataGenerationInterval)) {
            this.freq = this.freq * 2;
            console.log('Frequência aumentada : f = ' + this.freq);
        }
        else {
            console.log('Frequência não pode ser aumentada. Não obedece critério de Nyquist');
        }
    }

    freqHalf() {
        this.freq = this.freq / 2;
        console.log('Frequência reduzida : f = ' + this.freq);
    }

}
